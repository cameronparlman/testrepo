all: compile run clean

compile:
	javac test.java

run:
	java test

clean:
	rm -f test.class conc/concat.class
